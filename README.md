# `GTK` sweet theme

Sweet theme for `GTK` by [Eliver Lara](https://github.com/EliverLara).

## Install

Copy the contents of [sweet](sweet) into `/usr/share/themes` and set the theme
by adding the following into `~/.config/gtk-3.0/settings.ini`:
```
[Settings]
gtk-theme-name=sweet
```
